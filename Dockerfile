from ubuntu:20.04

run apt update
run apt install openssh-server -y
run apt install rsyslog nano sudo htop curl bash-completion iputils-ping python3 python3-pip nodejs -y
run DEBIAN_FRONTEND=noninteractive apt install xfce4 -y
run DEBIAN_FRONTEND=noninteractive apt install xrdp -y

run echo "pass\npass" | passwd root
run echo "Set disable_coredump false" > /etc/sudo.conf
run sed -r 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config  > /etc/ssh/sshd_config_new
run mv /etc/ssh/sshd_config_new /etc/ssh/sshd_config
run useradd -m -G sudo -s /bin/bash user
run echo "rehjr16\nrehjr16" | passwd user

run apt install software-properties-common -y
run add-apt-repository ppa:saiarcot895/chromium-beta && apt update && apt install chromium-browser -y
run apt install xfce4-terminal -y && echo "2" | update-alternatives --config x-terminal-emulator

run echo "env -u SESSION_MANAGER -u DBUS_SESSION_BUS_ADDRESS xfce4-session" > /root/.xsession
run echo "env -u SESSION_MANAGER -u DBUS_SESSION_BUS_ADDRESS xfce4-session" > /home/user/.xsession
run echo "mode: off" > /home/user/.xscreensaver

run sed -r 's/allowed_users=console/allowed_users=anybody/g' /etc/X11/Xwrapper.config > /etc/X11/Xwrapper.config_new
run mv /etc/X11/Xwrapper.config_new /etc/X11/Xwrapper.config

copy startup.sh /etc

ENTRYPOINT ["/bin/bash", "/etc/startup.sh"]
