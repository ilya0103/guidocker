#/bin/bash

cleanup() {
	echo Exitting conteiner by Docker stop $(date)
	
	stopdate=$(date +%s)
	ps -aux > /var/log/dockerstop_ps_$stopdate.stage1.log
	
	service xrdp stop
	service ssh stop
	service dbus stop
	service rsyslog stop
	
	sleep 5
	ps -aux > /var/log/dockerstop_ps_$stopdate.stage2.log
	sleep 1
	echo Bye Bye
	exit 0
}

trap 'cleanup' SIGTERM

service rsyslog start
service dbus start
service ssh start
service xrdp start

while true
do
sleep 1 & wait $!
done
